import time
from gpiozero import MCP3008

def raw_to_voltage(raw_val):
    result = raw_val*(3.3/1024)
    return result

adc = MCP3008(channel=0, clock_pin=11, mosi_pin=10, miso_pin=9, select_pin=8)

while True:
    print(f'ADC RAW value is: {adc.raw_value}')
    print(f'Voltage is: {raw_to_voltage(adc.raw_value)}')
    time.sleep(0.5)
# mcp3008-gpiozero

Example showing how to use the MCP3008 ADC with gpiozero library

# Development usage

You need Python above version 3.5, pip installed and SPI enabled on the Raspberry Pi 

1. Clone the repository `git clone git@gitlab.com:npes-py-experiments/mcp3008-gpiozero.git` 
2. Create a virtual environment https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/#creating-a-virtual-environment 
2. Activate the virtual environment https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/#activating-a-virtual-environment
3. Install requirements `pip install -r requirements.txt`
4. Run `python3 app.py` (linux) or `py app.py` (windows)